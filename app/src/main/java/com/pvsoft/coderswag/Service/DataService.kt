package com.pvsoft.coderswag.Service

import com.pvsoft.coderswag.Model.Category
import com.pvsoft.coderswag.Model.Product

/**
 * Created by minhp on 1/4/2018.
 */
object DataService {
    val categories = listOf<Category>(
            Category("SHIRTS","shirtimage"),
            Category("HATS","hatimage"),
            Category("HOODIES","hoodieimage"),
            Category("DIGITALS","digitalgoodsimage")
    )

    val shirts = listOf<Product>(
            Product("Shirt 1","32$", "shirt1"),
            Product("Shirt 2","12$", "shirt2"),
            Product("Shirt 3","52$", "shirt3")
    )

    val hats = listOf<Product>(
            Product("Hat 1","32$", "hat1"),
            Product("Hat 2","12$", "hat2"),
            Product("Hat 3","52$", "hat3")
    )

    val hoodies = listOf<Product>(
            Product("Hoodie 1","32$", "hoodie1"),
            Product("Hoodie 2","12$", "hoodie2"),
            Product("Hoodie 3","52$", "hoodie3")
    )

    val digitals = listOf<Product>(
            Product("Digital 1","32$", "digital1"),
            Product("Digital 2","12$", "digital2"),
            Product("Digital 3","52$", "digital3")
    )

    fun getProductsByCategory(categoryName: String): List<Product> {
        when(categoryName){
            "SHIRTS" -> return shirts
            "HATS" -> return hats
            "HOODIES" -> return hoodies
            else -> {
                return digitals
            }
        }
    }
}