package com.pvsoft.coderswag.Adapters

import android.content.Context
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.pvsoft.coderswag.Model.Category
import com.pvsoft.coderswag.R
import com.pvsoft.coderswag.R.id.categoryImage
import com.pvsoft.coderswag.R.id.categoryName

/**
 * Created by minhp on 1/4/2018.
 */
class CategoryAdapter constructor(context :Context, categoriesList: List<Category>): BaseAdapter() {

    val context = context
    val categoriesList = categoriesList

    override fun getCount(): Int {
        return categoriesList.count()
    }

    override fun getItemId(p0: Int): Long {
        return 0
    }

    override fun getItem(p0: Int): Any {
        return categoriesList[p0]
    }

    override fun getView(position: Int, convertView: View?, p2: ViewGroup?): View {
        val viewHolder: ViewHolder
        val categoryInflater: View
        if(convertView == null) {
            categoryInflater = LayoutInflater.from(context).inflate(R.layout.category_list_item, null)
            viewHolder = ViewHolder()
            viewHolder.categoryImage = categoryInflater.findViewById<ImageView>(R.id.categoryImage)
            viewHolder.categoryTitle = categoryInflater.findViewById<TextView>(R.id.categoryName)

            categoryInflater.tag = viewHolder
        } else {
            viewHolder = convertView.tag as ViewHolder
            categoryInflater = convertView
        }

        val resourceIdOfImqage = context.resources.getIdentifier(categoriesList[position].image,"drawable",context.packageName)
        viewHolder.categoryImage?.setImageResource(resourceIdOfImqage)
        viewHolder.categoryTitle?.text = categoriesList[position].title

        return categoryInflater
    }

    private class ViewHolder() {
        var categoryImage: ImageView? = null
//            get() = this.categoryImage

        var categoryTitle: TextView? = null
//            get() = this.categoryTitle
    }
}