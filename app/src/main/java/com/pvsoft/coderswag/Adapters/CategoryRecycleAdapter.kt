package com.pvsoft.coderswag.Adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.pvsoft.coderswag.Model.Category
import com.pvsoft.coderswag.R

/**
 * Created by minhp on 1/6/2018.
 */
class CategoryRecycleAdapter constructor(context : Context, categoriesList: List<Category>, val onItemClick: (Category) -> Unit) : RecyclerView.Adapter<CategoryRecycleAdapter.ViewHolder>() {

    var context = context
    var categoriesList = categoriesList

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.category_list_item,parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return categoriesList.count()
    }

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
//        holder?.categoryImage?.setImageResource(context.resources.getIdentifier(categoriesList[position].image,"drawable",context.packageName))
//        holder?.categoryName?.text = categoriesList[position].title
        holder?.bindData(context,categoriesList[position],onItemClick)
    }


    class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        val categoryName = itemView?.findViewById<TextView>(R.id.categoryName)
        val categoryImage = itemView?.findViewById<ImageView>(R.id.categoryImage)

        fun bindData(context: Context, category: Category, onItemClick: (Category) -> Unit) {
            categoryName?.text = category.title
            val imageResourceId = context.resources.getIdentifier(category.image,"drawable",context.packageName)
            categoryImage?.setImageResource(imageResourceId)
//            itemView.setOnClickListener { onItemClick(category) }
            itemView.setOnClickListener(View.OnClickListener { onItemClick(category) })
        }
    }
}

