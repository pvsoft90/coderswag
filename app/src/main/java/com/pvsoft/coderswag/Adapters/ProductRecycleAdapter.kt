package com.pvsoft.coderswag.Adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.pvsoft.coderswag.Model.Product
import com.pvsoft.coderswag.R

/**
 * Created by minhphuongnguyen on 1/6/18.
 */
class ProductRecycleAdapter constructor(context: Context, productList: List<Product>): RecyclerView.Adapter<ProductRecycleAdapter.ViewHolder>() {

    val context =context
    val productList = productList

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.product_list_item,parent,false))
    }

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        holder?.bindData(context, productList[position])
    }

    override fun getItemCount(): Int {
        return productList.count()
    }


    inner class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        val productName = itemView?.findViewById<TextView>(R.id.productNameTxt)
        val productPrice = itemView?.findViewById<TextView>(R.id.productPriceTxt)
        val productImage = itemView?.findViewById<ImageView>(R.id.productImg)

        fun bindData(context: Context, product: Product){
           val imageResourceId = context.resources.getIdentifier(product.image,"drawable",context.packageName)
            productImage?.setImageResource(imageResourceId)
            productName?.text = product.title
            productPrice?.text = product.price
        }
    }
}