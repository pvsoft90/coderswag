package com.pvsoft.coderswag.Controller

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.View
import android.widget.ArrayAdapter
import com.pvsoft.coderswag.Adapters.CategoryAdapter
import com.pvsoft.coderswag.Adapters.CategoryRecycleAdapter
import com.pvsoft.coderswag.Model.Category
import com.pvsoft.coderswag.R
import com.pvsoft.coderswag.Service.DataService
import com.pvsoft.coderswag.Utilities.EXTRA_CATEGORY
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

//    lateinit var categoriesAdapter: CategoryAdapter

    lateinit var categoryRecycleAdapter: CategoryRecycleAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

//        categoriesAdapter = CategoryAdapter(this, DataService.categories)
//        categoriesListView.adapter = categoriesAdapter

        categoryRecycleAdapter = CategoryRecycleAdapter(this,DataService.categories) {
            item: Category -> val productIntent = Intent(this,ProductActivity::class.java)
            productIntent.putExtra(EXTRA_CATEGORY,item.title)
            startActivity(productIntent)
        }
        categoriesRecyclerView.adapter = categoryRecycleAdapter
        var recyclerViewLayout = LinearLayoutManager(this)
        categoriesRecyclerView.layoutManager = recyclerViewLayout
        categoriesRecyclerView.setHasFixedSize(true)
    }
}
