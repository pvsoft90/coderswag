package com.pvsoft.coderswag.Controller

import android.content.res.Configuration
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.pvsoft.coderswag.Adapters.ProductRecycleAdapter
import com.pvsoft.coderswag.Model.Product
import com.pvsoft.coderswag.R
import com.pvsoft.coderswag.Service.DataService
import com.pvsoft.coderswag.Utilities.EXTRA_CATEGORY
import kotlinx.android.synthetic.main.activity_product.*

class ProductActivity : AppCompatActivity() {

    lateinit var productRecycleAdapter: ProductRecycleAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product)

        val categoryTitle = intent.getStringExtra(EXTRA_CATEGORY)
        val productList: List<Product> = DataService.getProductsByCategory(categoryTitle)
        productRecycleAdapter = ProductRecycleAdapter(this,productList)
        productRecyclerView.adapter = productRecycleAdapter

        var spanCount = 2
        val orientation = this.resources.configuration.orientation
        if(orientation == Configuration.ORIENTATION_LANDSCAPE) {
            spanCount = 3
        }

        val screenSize = resources.configuration.screenWidthDp
        if(screenSize > 720) {
            spanCount = 3
        }

        val layoutManager = GridLayoutManager(this, spanCount)
        productRecyclerView.layoutManager = layoutManager
        productRecyclerView.setHasFixedSize(true)
    }
}
